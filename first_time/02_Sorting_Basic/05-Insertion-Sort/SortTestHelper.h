//
// Created by SunYing on 2017/11/2.
//

#ifndef CPP_JAVA_LEARNING_SORTTESTHELPER_H
#define CPP_JAVA_LEARNING_SORTTESTHELPER_H

#include <iostream>
#include <cassert>
using namespace std;

//namespace SortTestHelper{
//    int *generateRandomArray(int n, int rang_l, int rang_r){
////        初始化数组的时候不能使用[]
//        int *arr = new int[n];
//
//        srand(time(NULL));
//
//        for (int i = 0; i < n; ++i) {
//            arr[i] = rand() % (rang_r - rang_l + 1) + rang_l;
//        }
//        return arr;
//    }
//
//    int *copyIntArray(int a[], int n){
//        int *arr = new int[n];
//
//        copy(a, a + n, arr);
//        return arr;
//    }
//
//    template<typename T>
//    void printArray(T arr[], int n){
//        for (int i = 0; i < n; ++i) {
//            cout << arr[i] << " ";
//        }
//        cout << endl;
//        return;
//    }
//
//    template <typename T>
//    bool isSorted(T arr[], int n){
//        for (int i = 0; i < n - 1; ++i) {
//            if (arr[i] > arr[i + 1]) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    template<typename T>
//    void testSort(const string &sortName, void (*sort)(T[], int), T arr[], int n){
//        clock_t startTime = clock();
//        sort(arr, n);
//        clock_t endTime = clock();
//        cout << sortName << " : " << double(endTime - startTime) / CLOCKS_PER_SEC << " s" << endl;
//
//        assert(isSorted(arr, n));
//
//        return;
//    }
//}
#endif //CPP_JAVA_LEARNING_SORTTESTHELPER_H
