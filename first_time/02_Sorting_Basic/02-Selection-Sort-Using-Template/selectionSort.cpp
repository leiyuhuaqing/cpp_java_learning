
#include <iostream>
#include "Student.h"

using namespace std;

template <typename T>
void selectionSort(T arr[], int n){
//    for (int i = 0; i < n; ++i) {
//        //这里的最小的变量一定是i,不能是0,因为每次循环的时候最小值从第i个开始然后变量是i+1后面到n的所有
////        数值和这个i来进行比较
//        int minIndex = i;
//        for (int j = i + 1; j < n; ++j) {
//            if (arr[j] < arr[minIndex]) {
//                minIndex = j;
//            }
//        }
//        swap(arr[i], arr[minIndex]);
//    }

    for (int i = 0; i < n; ++i) {
        int minIndex = i;
        for (int j = i + 1; j < n; ++j) {
            if (arr[j] < arr[minIndex]) {
                minIndex = j;
            }
        }
//        这里怎么替换都无所谓
        swap(arr[minIndex], arr[i]);
//        swap(arr[i], arr[minIndex]);
    }

}

//int main() {
//    // 测试模板函数，传入整型数组
//    int a[10] = {10,2,5,7,2,7,33,6,10,10};
//    selectionSort(a, 10);
//    for (int i = 0; i < 10; ++i) {
//        cout << a[i] << " ";
//    }
//    cout << endl;
//
//    // 测试模板函数，传入浮点型数组
//    float b[10] = {10.2,2.4,5,7,2,7,33.7,6,10,10};
//    selectionSort(b, 10);
//    for (int i = 0; i < 10; ++i) {
//        cout << b[i] << " ";
//    }
//    cout << endl;
//    // 测试模板函数，传入字符串数组
//    string c[4] = {"D", "C", "B", "A"};
//    selectionSort(c, 4);
//    for (int j = 0; j < 4; ++j) {
//        cout << c[j] << " ";
//    }
//    cout << endl;
//
//    Student d[4] = {{"D", 90},
//                    {"C", 100},
//                    {"B", 95},
//                    {"A", 95}};
//    selectionSort(d, 4);
//    for (int k = 0; k < 4; ++k) {
//        cout << d[k];
//    }
//    cout << endl;
//
//    return 0;
//}