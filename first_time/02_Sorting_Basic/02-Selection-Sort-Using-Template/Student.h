//
// Created by SunYing on 2017/11/1.
//

#ifndef CPP_JAVA_LEARNING_STUDENT_H
#define CPP_JAVA_LEARNING_STUDENT_H

// iostream 必须和using namespace std;共同使用
#include <iostream>
//下面引用了std之后这里也可以注释掉
//#include <string>

using namespace std;

struct Student{
    string name;
    int score;

    // 重载小于运算法,定义Student之间的比较方式
    bool operator<(const Student &otherStudent) {

    // 如果分数相等，则按照名字的字母序排序
    // 如果分数不等，则分数高的靠前
        return score != otherStudent.score ?
               score > otherStudent.score : name < otherStudent.name;
    }

    // 重载<<符号, 定义Student实例的打印输出方式
    // * 很多同学看到这里的C++语法, 头就大了, 甚至还有同学表示要重新学习C++语言
    // * 对于这个课程, 大可不必。C++语言并不是这个课程的重点,
    // * 大家也完全可以使用自己的方式书写代码, 最终只要能够打印出结果就好了, 比如设置一个成员函数, 叫做show()...
    // * 推荐大家阅读我在问答区向大家分享的一个学习心得: 【学习心得分享】请大家抓大放小，不要纠结于C++语言的语法细节
    // * 链接: http://coding.imooc.com/learn/questiondetail/4100.html

//    friend 这个的意思是.....
    friend ostream &operator<<(ostream &os, const Student &student){
//      前面是个指针变量,然后通过往这个变量里面传入数据组成一段输出
        os << "Student: " << student.name << " " << student.score << endl;
        return os;
    }

};

#endif //CPP_JAVA_LEARNING_STUDENT_H
